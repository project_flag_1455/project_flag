from config import *
from functions.get_user import *
from functions.verify_injection import *
from functions.cripto_pass import *
from functions.verify_injection_email import *

def atualizar_user(dados):
    try:
        user = get_usuario(dados['email'])
        if user is not None:
            if 'novo_nome' in dados:
                user.nome = verifica_injecao(dados['novo_nome'])
            if 'senha' in dados:
                senha = verifica_injecao(dados['senha']) 
                user.senha = criptografar_sen(senha) 
            if 'email' in dados:
                user.email = verifica_injecao_email(dados['novo_email'])
            if dados is None:
                return False
            db.session.commit()
            return True
        return False
    except Exception as e:
        return False
