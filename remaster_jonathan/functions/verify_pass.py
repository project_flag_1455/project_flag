from config import *
from model.usuario import *
import bcrypt

def verifica_senha(senha_dig:str,email_dig:str):
    """Verifica se a senha digitada corresponde a que está no banco de dados
    usando o email de referência.

    Args:
        senha_dig (str): senha digitada.
        email_dig (str): email digitado.

    Returns:
        bool: False caso aconteça algum erro ou caso a senha não esteja no banco;
        True caso esteja e corresponda.
    """
    for q in db.session.query(Usuario.senha).filter(Usuario.id_email==email_dig).all():
        try:
            resultado = bcrypt.checkpw(senha_dig,q[0])
        except:
            return False
        if q == None:
            return False
        return resultado