from config import *

@app.route('/')
def home():
    '''Rota inicial do site

    Returns:
        index(html): retorna a página inicial 
    '''
    return render_template('index.html')

@app.route('/perfil', methods=['GET'])
def profile():
    '''Rederiza a página de perfil

    Returns:
        perfil(html): página de perfil
    '''
    return render_template('perfil.html')

@app.route('/render_menu', methods=['GET'])
def niveis():
    '''Renderiza a página de niveis

    Returns:
        niveis(html): Página de niveis
    '''
    return render_template('menu.html')