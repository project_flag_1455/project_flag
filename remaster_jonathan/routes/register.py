from config import *
from model.usuario import *
from functions.cripto_pass import *
from functions.verify_injection import *
from functions.verify_pass import *
from functions.get_user import *

@app.route('/cadastro', methods=['GET','POST'])
def cadastro():
    if request.method == 'GET':
        return render_template('cadastro.html')
    else:
        try:
            dados = request.get_json(force=True)
            user = Usuario(id_email =  dados['email'],nome = dados['nome'],senha = dados['senha'])
            for f in filtro:
                if f in user.nome or f in user.id_email or f in user.senha:
                    user.nome = user.nome.replace(f,'')
                    user.id_email = user.id_email.replace(f,'')
                    user.senha = user.senha.replace(f,'')
            if user.id_email == '' or user.nome == '' or user.senha == '':
                resposta = jsonify({'Resultado': 'nulo'})
                resposta.headers.add('Access-Control-Allow-Origin', '*')
                return resposta
            if '@' not in user.id_email and len(user.id_email)<=6:
                resposta = jsonify({'Resultado':'email inv'})
                resposta.headers.add('Access-Control-Allow-Origin', '*')
                return resposta
            a = db.session.query(Usuario.id_email).filter_by(id_email = user.id_email).first()
            if a is not None:
                resposta = jsonify({'Resultado':'Usuário já cadastrado'})
                resposta.headers.add('Access-Control-Allow-Origin', '*')
                return resposta
            user.senha = criptografar_sen(user.senha)
            db.session.add(user) # Adiciona o usuário na tabela.
            db.session.commit()
            resposta = jsonify({'Resultado': 'sucesso', 'Detalhes': 'ok'})
        except Exception as e:
            # Retorna um erro de cadastro caso ocorra.
            resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)}) 
        resposta.headers.add('Access-Control-Allow-Origin', '*')
        return resposta

@app.route('/login', methods= ['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        try:
            dados = request.get_json(force=True) #Pega os dados do formulario.
            email = str(dados['email'])
            senha = str(dados['senha'])
            for f in filtro: # laço de repetição que verifica se não há um texto suspeto de possuir injeção XSS ou SQL.
                if f in email or f in senha:
                    email = email.replace(f,'')
                    senha = senha.replace(f,'')
            senha = senha.encode('utf-8') # Deixa a senha no padrão utf-8.
            login = verifica_senha(senha,email) # Função que verifica a existencia do usuário.
            user = get_usuario(email) # Select que pega o email e nome do usuário.
            email = user.id_email
            nome = user.nome
            jw_token = create_access_token(identity=email)
            resposta = jsonify({'Resultado': 'sucesso', 'Detalhes': jw_token, 'nome':nome,'email':email})
            resposta.headers.add('Access-Control-Allow-Origin', '*')
            if login == False:
                resposta = jsonify({'Resultado': 'senha inv'})
        except Exception as e:
            # Retorna um erro de cadastro caso ocorra.
            resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta
