from config import *
from model.usuario import *
from functions.verify_injection import *
from functions.cripto_pass import *
from functions.update_user import *

@app.route('/update')
def update():
    '''Renderiza a página de update

    Returns:
        updates(html): retorna a página de update
    '''
    return render_template('updates.html')

@app.route('/update_email', methods=['PUT'])
@jwt_required()
def update_email():
    '''Atualiza o Email.

    Returns:
        resposta(json): Retorna sucesso se o email for atualizado.
    '''
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario
        email_user = dados['email']
        email_novo = str(dados['novo_email'])
        for f in filtro: # laço de repetição que verifica se não há um texto suspeto de possuir injeção XSS ou SQL.
            if f in email_novo:
                email_novo = email_novo.replace(f,'')
        if email_novo == '' and len(email_novo)<=4 or '@' not in email_novo:
            resposta = jsonify({'Resultado':'nulo'})
            return resposta
        db.session.query(Usuario).filter_by(id_email=email_user).update(dict(id_email=email_novo)) # Atualiza o email do usuário.
        db.session.commit()
        user = db.session.query(Usuario.id_email).filter_by(id_email=email_novo).first() # Retorna uma lista com o novo email.
        email = user[0] # Tira o email da lista
        resposta = jsonify({'Resultado': 'sucesso','email':email})
    except Exception as e:
        # Retorna um erro de cadastro caso ocorrá.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta


@app.route('/update_nome', methods=['PUT'])
@jwt_required()
def update_nome():
    '''Atualiza o nome do usuário.

    Returns:
        resposta(json): Retorna sucesso se o nome for atualizado.
    '''
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario
        user = str(dados['novo_nome'])
        for f in filtro: # laço de repetição que verifica se não há um texto suspeito de possuir injeção XSS ou SQL.
            if f in user:
                user = user.replace(f,'')
        if dados['novo_nome'] == '':
            resposta = jsonify({'Resultado':'nulo'})
            return resposta
        email_user = dados['email']
        db.session.query(Usuario).filter_by(id_email=email_user).update(dict(nome=user)) # Realiza o update do nome
        db.session.commit()
        user = db.session.query(Usuario.nome).filter_by(id_email=email_user).first() # Retorna uma lista com o novo nome.
        nome = user[0] # Retira o nome de uma lista
        resposta = jsonify({'Resultado': 'sucesso','nome':nome})
        resposta.headers.add('Access-Control-Allow-Origin', '*')
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)}) 
        return resposta
    return resposta

@app.route('/update_senha', methods=['PUT'])
@jwt_required()
def update_senha():
    '''Atualiza a senha.

    Returns:
        resposta(json): Retorna sucesso se o nome for atualizado.
    '''
    resposta = jsonify({'Resultado': 'sucesso'})
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario.
        email_user = dados['email']
        senha_new = str(dados['nova_senha'])
        senha_ver = verifica_injecao(senha_new) # função que verifica se não há um texto suspeito de possuir injeção XSS ou SQL.
        if senha_ver is None:
            resposta = jsonify({'Resultado':'nulo'})
        else: 
            senha_new = criptografar_sen(senha_ver)  # função que criptografa a senha 

        db.session.query(Usuario).filter_by(id_email=email_user).update(dict(senha=senha_new)) # Atualiza a senha.
        db.session.commit()
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta

@app.route('/update_user', methods=['PUT'])
@jwt_required()
def update_user():
    '''Atualiza o Usuario.

    Returns:
        resposta(json): Retorna sucesso se o email for atualizado.
    '''
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario
        if atualizar_user(dados):
            resposta = jsonify({'Resultado': 'sucesso'})
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta
