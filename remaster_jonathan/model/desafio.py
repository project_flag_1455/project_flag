from config import *

class Desafio(db.Model):
    """Representa um desafio. 
    Herda de db.Model que é a classe base para todos os modelos no SQLAlchemy.
    """
    id = db.Column(db.Integer,primary_key=True)
    nome = db.Column(db.String(254))
    password = db.Column(db.String(254))
    pont_ger = db.Column(db.Integer,default = 0) # Pontuação Geral do desafio.
    pont_cript = db.Column(db.Integer,default = 0) # Pontuação Criptografia do desafio.
    pont_estgn = db.Column(db.Integer,default = 0) # Pontuação Estegnografia do desafio.
    pont_char = db.Column(db.Integer,default = 0) # Pontuação de Charadas do desasfio.

    def __str__(self):
        return f'{self.id},{self.nome},{self.password}'