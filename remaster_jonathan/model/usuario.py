from config import *
from model.desafio import *

class Usuario(db.Model):
    """Representa um usuário. 
    Herda de db.Model que é a classe base para todos os modelos no SQLAlchemy.
    """
    id_email = db.Column(db.String(254), primary_key=True)
    nome = db.Column(db.String(254))
    senha = db.Column(db.String(254))
    pont_ger = db.Column(db.Integer,nullable = True,default = 0) # Pontuação Geral.
    pont_cript = db.Column(db.Integer,nullable = True,default = 0) # Pontuação Criptografia.
    pont_estgn = db.Column(db.Integer,nullable = True,default = 0) # Pontuação Estegnografia.
    pont_char = db.Column(db.Integer,nullable = True,default = 0) # Pontuação de Charadas.
    desafio_atual = db.Column(db.Integer,db.ForeignKey(Desafio.id), nullable = False,default = 1)
    

    def __str__(self):
        """Retorna o objeto em string."""
        return f"{self.id_email},{self.nome},{self.senha},{self.pont_ger},{self.pont_cript},{self.pont_estgn},{self.pont_char}"

    def json(self):
        """Retorna o objeto no formato json."""
        return {
            'email':self.id_email,
            'nome':self.nome
        }
    
    def ret_pont(self):
        """Retorna as pontuações em formato json."""
        return{
            'pont_ger':self.pont_ger,
            'pont_cript':self.pont_cript,
            'pont_estgn':self.pont_estgn,
            'pont_char':self.pont_char
        }