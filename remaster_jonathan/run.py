from config import *
from routes.default import *
from routes.register import *
from routes.generate_ranking import *
from routes.get_user import *
from routes.load_challenge import *
from routes.update import *
from routes.niveis import *

app.run(debug = True, host='0.0.0.0')
#serve(TransLogger(app, setup_console_handler=False), host='0.0.0.0', port=5000)