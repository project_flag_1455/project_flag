$(function(){

        document.getElementById('conteudo').innerHTML += `
                <p class="paragraph">
                É rápido como uma <b>s</b>ombra, curto como um sonho, <br>
                breve como um r<b>e</b>lâmpago na noite fria, <br>
                que com melancolia revela ta<b>n</b>to o céu quanto a terra, <br>
                e antes que o <b>h</b>omem consiga dizer "Veja!", <br>
                os dentes da noite o devoram. <br>
                E assim, depressa, tudo o que é luminoso, <br>
                desaparece em meio à perplexid<b>a</b>de. <br><br>
                <em id="smp">- Shakespeare, William</em>
            </p>`

    $(document).on("click",'#responder',function(){
        var resp = prompt("Digite a senha:");
        user_email = sessionStorage.getItem('email');
        meuip = sessionStorage.getItem('meuip');
        jwt = sessionStorage.getItem('jwt')

        var log = JSON.stringify({ email: user_email, resposta: resp });

        $.ajax({
            url: `http://${meuip}:5000/nivel1`,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            headers: { Authorization: 'Bearer ' + jwt },
            data: log, 
            success: sucesso,
            error: erroAoResponder
    });
    function sucesso (retorno){
        alert(`${retorno.Resultado}`)
        if (retorno.Resultado === 'certa'){
            alert('Resposta correta');
            window.location.assign('/desafio2');
        }else{
            alert('Resposta errada!');
        }
    };
    function erroAoResponder(retorno){
            window.alert('Ocorreu um erro no backend',retorno.erro)
    }
});
});