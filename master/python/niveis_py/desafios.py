from flask import jsonify, request
from config import *
from modelo import Desafio, Usuario, filtro


@app.route('/desafio1',methods=['GET'])
def render_desafio1():
    return render_template('desafio1.html')

@app.route('/desafio2',methods=['GET'])
def render_desafio2():
    return render_template('desafio2.html')

@app.route('/desafio3',methods=['GET'])
def render_desafio3():
    return render_template('desafio3.html')

@app.route('/desafio4',methods=['GET'])
def render_desafio4():
    return render_template('desafio4.html')

@app.route('/desafio5',methods=['GET'])
def render_desafio5():
    return render_template('desafio5.html')

@app.route('/desafio6',methods=['GET'])
def render_desafio6():
    return render_template('desafio6.html')

"""@app.route('/desafiox',methods=['GET'])
def render_desafiox():
    return render_template('desafiox.html')"""

