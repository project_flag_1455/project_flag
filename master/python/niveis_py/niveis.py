from flask import jsonify, request
from config import *
from modelo import Desafio, Usuario, filtro

@app.route('/nivel1', methods=['PUT'])
@jwt_required()
def nivel1():
    resposta = jsonify({'Resposta':'errada'})
    dados = request.get_json()
    try:
        answer= str(dados['resposta'])
        for f in filtro:
            if f in answer:
                answer = resposta.replace(f,'')
        user = Usuario.query.get(dados['email'])
        print(answer)
        if answer == 'senha' or answer == 'Senha' or answer == 'SENHA' : #A resposta do desafio não foi definida ainda.
            desafio = Desafio.query.get(1)
            if user.desafio_atual == desafio.id:
                user.pont_ger += desafio.pont_ger
                user.pont_cript += desafio.pont_cript
                user.pont_char += desafio.pont_char
                user.pont_estgn += desafio.pont_estgn
                user.desafio_atual += 1
                resposta = jsonify({"Resultado":"certa"})
                db.session.commit()
                return resposta
            else:
                resposta = jsonify({'Resposta':'erro','Detalhes':'Usuário já passou ou não esta no desafio certo'})
                return resposta
    except Exception as e:
        resposta = jsonify({'Resposta':'erro','Detalhes':str(e)})
        return resposta
    resposta.headers.add('Access-Control-Allow-Origin','*')
    return resposta

@app.route("/nivel2",methods=['PUT'])
@jwt_required()
def nivel2():
    resposta = jsonify({'Resposta':'errada'})
    dados = request.get_json()
    try:
        answer= str(dados['resposta'])
        for f in filtro:
            if f in answer:
                answer = resposta.replace(f,'')
        user = Usuario.query.get(dados['email'])
        if answer == 'culpado' or answer == 'Culpado' or answer == 'CULPADO': #A resposta do desafio não foi definida ainda.
            desafio = Desafio.query.get(2)
            if user.desafio_atual == desafio.id:
                user.pont_ger += desafio.pont_ger
                user.pont_cript += desafio.pont_cript
                user.pont_char += desafio.pont_char
                user.pont_estgn += desafio.pont_estgn
                user.desafio_atual += 1
                resposta = jsonify({"Resultado":"certa"})
                db.session.commit()
                return resposta
            else:
                resposta = jsonify({'Resposta':'erro','Detalhes':'Usuário já passou ou não esta no desafio certo'})
                return resposta
    except Exception as e:
        resposta = jsonify({'Resposta':'erro','Detalhes':str(e)})
        return resposta
    resposta.headers.add('Access-Control-Allow-Origin','*')
    return resposta

@app.route("/nivel3",methods=['PUT'])
@jwt_required()
def nivel3():
    resposta = jsonify({'Resposta':'errada'})
    dados = request.get_json()
    try:
        answer= str(dados['resposta'])
        for f in filtro:
            if f in answer:
                answer = resposta.replace(f,'')
        user = Usuario.query.get(dados['email'])
        if answer == 'sinfonia': #A resposta do desafio não foi definida ainda.
            desafio = Desafio.query.get(3)
            if user.desafio_atual == desafio.id:
                user.pont_ger += desafio.pont_ger
                user.pont_cript += desafio.pont_cript
                user.pont_char += desafio.pont_char
                user.pont_estgn += desafio.pont_estgn
                user.desafio_atual += 1
                resposta = jsonify({"Resultado":"certa"})
                db.session.commit()
                return resposta
            else:
                resposta = jsonify({'Resposta':'erro','Detalhes':'Usuário já passou ou não esta no desafio certo'})
                return resposta
    except Exception as e:
        resposta = jsonify({'Resposta':'erro','Detalhes':str(e)})
        return resposta
    resposta.headers.add('Access-Control-Allow-Origin','*')
    return resposta