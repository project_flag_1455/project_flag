from config import *
from modelo import *

@app.route('/update_user', methods=['PUT'])
@jwt_required()
def update_user():
    '''Atualiza o Usuario.

    Returns:
        resposta(json): Retorna sucesso se o email for atualizado.
    '''
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario
        if atualizar_user(dados):
            resposta = jsonify({'Resultado': 'sucesso'})
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta
