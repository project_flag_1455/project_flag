from config import *
from modelo import *

@app.route('/get_user', methods=['POST'])
@jwt_required()
def get_user():
    resposta = jsonify({'Resultado':'ok'})
    dados= request.get_json(force=True)
    try:
        email = dados['email']
        user = get_usuario(email)
    except Exception as e:
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta
    resposta = jsonify({'Resultado':'sucesso','estn':str(user.pont_estgn),'cript':str(user.pont_cript),'ger':str(user.pont_ger),'char':str(user.pont_char)})
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta
