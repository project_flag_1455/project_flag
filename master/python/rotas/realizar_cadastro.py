from config import *
from modelo import *

@app.route('/realizar_cadastro', methods=['POST'])
def realizar_cadastro():
    '''Função que realiza o cadastro

    Returns:
        resposta(json): Retorna sucesso se o usuário for cadastrado.
    '''
    try:
        dados = request.get_json(force=True)
        user = Usuario(id_email =  dados['email'],nome = dados['nome'],senha = dados['senha'])
        for f in filtro:
            if f in user.nome or f in user.id_email or f in user.senha:
                user.nome = user.nome.replace(f,'')
                user.id_email = user.id_email.replace(f,'')
                user.senha = user.senha.replace(f,'')
        if user.id_email == '' or user.nome == '' or user.senha == '':
            resposta = jsonify({'Resultado': 'nulo'})
            resposta.headers.add('Access-Control-Allow-Origin', '*')
            return resposta
        if '@' not in user.id_email and len(user.id_email)<=6:
            resposta = jsonify({'Resultado':'email inv'})
            resposta.headers.add('Access-Control-Allow-Origin', '*')
            return resposta
        a = db.session.query(Usuario.id_email).filter_by(id_email = user.id_email).first()
        if a is not None:
            resposta = jsonify({'Resultado':'Usuário já cadastrado'})
            resposta.headers.add('Access-Control-Allow-Origin', '*')
            return resposta
        user.senha = criptografar_sen(user.senha)
        db.session.add(user) # Adiciona o usuário na tabela.
        db.session.commit()
        resposta = jsonify({'Resultado': 'sucesso', 'Detalhes': 'ok'})
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)}) 
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta
