from config import *
from modelo import *

@app.route('/update_nome', methods=['PUT'])
@jwt_required()
def update_nome():
    '''Atualiza o nome do usuário.

    Returns:
        resposta(json): Retorna sucesso se o nome for atualizado.
    '''
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario
        user = str(dados['novo_nome'])
        for f in filtro: # laço de repetição que verifica se não há um texto suspeito de possuir injeção XSS ou SQL.
            if f in user:
                user = user.replace(f,'')
        if dados['novo_nome'] == '':
            resposta = jsonify({'Resultado':'nulo'})
            return resposta
        email_user = dados['email']
        db.session.query(Usuario).filter_by(id_email=email_user).update(dict(nome=user)) # Realiza o update do nome
        db.session.commit()
        user = db.session.query(Usuario.nome).filter_by(id_email=email_user).first() # Retorna uma lista com o novo nome.
        nome = user[0] # Retira o nome de uma lista
        resposta = jsonify({'Resultado': 'sucesso','nome':nome})
        resposta.headers.add('Access-Control-Allow-Origin', '*')
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)}) 
        return resposta
    return resposta
