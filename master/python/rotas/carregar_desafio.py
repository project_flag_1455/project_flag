from config import *
from modelo import *

@app.route('/carregar_desafio',methods=['POST'])
@jwt_required()
def render_nivel():
    try:
        dados = request.get_json(force=True)
        print(dados)
        email = dados['email']
        user = get_usuario(email)
        resposta = jsonify({'Resposta':'sucesso','desafio':user.desafio_atual})
    except Exception as e:
        resposta=jsonify({'Resposta':'erro','Detalhes':str(e)})
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta