from config import *
from modelo import *

@app.route('/realizar_login', methods= ['POST'])
def realizar_login():
    ''' Realiza o login do usuário.

    Returns:
        resposta(json): retorna sucesso caso o usuário seja autenticado.
    '''
    try:
        dados = request.get_json(force=True) #Pega os dados do formulario.
        email = str(dados['email'])
        senha = str(dados['senha'])
        for f in filtro: # laço de repetição que verifica se não há um texto suspeto de possuir injeção XSS ou SQL.
            if f in email or f in senha:
                email = email.replace(f,'')
                senha = senha.replace(f,'')
        senha = senha.encode('utf-8') # Deixa a senha no padrão utf-8.
        login = verifica_senha(senha,email) # Função que verifica a existencia do usuário.
        user = get_usuario(email) # Select que pega o email e nome do usuário.
        email = user.id_email
        nome = user.nome
        jw_token = create_access_token(identity=email)
        resposta = jsonify({'Resultado': 'sucesso', 'Detalhes': jw_token, 'nome':nome,'email':email})
        resposta.headers.add('Access-Control-Allow-Origin', '*')
        if login == False:
            resposta = jsonify({'Resultado': 'senha inv'})
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
    return resposta
