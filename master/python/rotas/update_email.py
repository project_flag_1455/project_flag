from config import *
from modelo import *

@app.route('/update_email', methods=['PUT'])
@jwt_required()
def update_email():
    '''Atualiza o Email.

    Returns:
        resposta(json): Retorna sucesso se o email for atualizado.
    '''
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario
        email_user = dados['email']
        email_novo = str(dados['novo_email'])
        for f in filtro: # laço de repetição que verifica se não há um texto suspeto de possuir injeção XSS ou SQL.
            if f in email_novo:
                email_novo = email_novo.replace(f,'')
        if email_novo == '' and len(email_novo)<=4 or '@' not in email_novo:
            resposta = jsonify({'Resultado':'nulo'})
            return resposta
        db.session.query(Usuario).filter_by(id_email=email_user).update(dict(id_email=email_novo)) # Atualiza o email do usuário.
        db.session.commit()
        user = db.session.query(Usuario.id_email).filter_by(id_email=email_novo).first() # Retorna uma lista com o novo email.
        email = user[0] # Tira o email da lista
        resposta = jsonify({'Resultado': 'sucesso','email':email})
    except Exception as e:
        # Retorna um erro de cadastro caso ocorrá.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta

