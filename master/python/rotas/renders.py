from config import *

@app.route('/')
def home():
    '''Rota inicial do site

    Returns:
        index(html): retorna a página inicial 
    '''
    return render_template('index.html')

@app.route('/update')
def update():
    '''Renderiza a página de update

    Returns:
        updates(html): retorna a página de update
    '''
    return render_template('updates.html')

@app.route('/render_menu', methods=['GET'])
def niveis():
    '''Renderiza a página de niveis

    Returns:
        niveis(html): Página de niveis
    '''
    return render_template('menu.html')

@app.route('/cadastro', methods= ['GET'])
def cadastro():
    '''Renderiza a página de cadastro

    Returns:
        cadastro(html): página de cadastro
    '''
    return render_template('cadastro.html')

@app.route('/login', methods= ['GET'])
def login():
    '''Renderiza a página de login.

    Returns:
        login(html): página de login
    '''
    return render_template('login.html')

@app.route('/perfil', methods=['GET'])
def profile():
    '''Rederiza a página de perfil

    Returns:
        perfil(html): página de perfil
    '''
    return render_template('perfil.html')
