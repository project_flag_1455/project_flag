from config import *
from modelo import *

@app.route('/ranking', methods=['GET'])
# @jwt_required()
def ranking():
    """Funça que gera um ranking com os melhores colocados.(Não está pronto.)

    Returns:
        resposta(Json): Retorna uma lista de usuários para o frontend.
    """
    resposta = jsonify({'resultado':'ok'})
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    try:
        for x in db.session.query(Usuario.nome).order_by(Usuario.pont_ger.desc()).slice(1,21):
            #Realiza uma consulta no banco que retorna uma lista dos melhores colocados.
            resposta.append({f'{x[0]}':f'{x[0]}'})
        #resposta = json.dumps(list) # Transforma a lista do python em uma lista json.
    except Exception as e: # Entra neste execpt caso ocorrá um erro.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta
    return resposta
