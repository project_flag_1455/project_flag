from config import *
from modelo import *

@app.route('/update_senha', methods=['PUT'])
@jwt_required()
def update_senha():
    '''Atualiza a senha.

    Returns:
        resposta(json): Retorna sucesso se o nome for atualizado.
    '''
    resposta = jsonify({'Resultado': 'sucesso'})
    try:
        dados = request.get_json(force=True) # Pega os dados do formulario.
        email_user = dados['email']
        senha_new = str(dados['nova_senha'])
        senha_ver = verifica_injecao(senha_new) # função que verifica se não há um texto suspeito de possuir injeção XSS ou SQL.
        if senha_ver is None:
            resposta = jsonify({'Resultado':'nulo'})
        else: 
            senha_new = criptografar_sen(senha_ver)  # função que criptografa a senha 

        db.session.query(Usuario).filter_by(id_email=email_user).update(dict(senha=senha_new)) # Atualiza a senha.
        db.session.commit()
    except Exception as e:
        # Retorna um erro de cadastro caso ocorra.
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    return resposta
