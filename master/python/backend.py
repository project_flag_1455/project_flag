from flask import *
from config import *
from niveis_py.niveis import *
from niveis_py.desafios import *
from rotas.renders import *
from rotas.carregar_desafio import *
from rotas.gera_ranking import *
from rotas.get_user import *
from rotas.realizar_cadastro import *
from rotas.realizar_login import *
from rotas.update_user import *
from rotas.update_nome import *
from rotas.update_senha import *
from rotas.update_email import *

if __name__ == '__main__':
    app.run(debug = True, host='0.0.0.0')
    #serve(TransLogger(app, setup_console_handler=False), host='0.0.0.0', port=5000)
    
