from config import *
from user import *
import bcrypt

def criptografar_sen(senha: str):
        """Criptografa a senha usando o bcrypt."""
        senha = senha.encode('utf-8') # Deixa a senha no padrão utf-8.
        nova_senha = bcrypt.hashpw(senha,bcrypt.gensalt()) # Gera a senha criptografada
        senha = nova_senha
        return senha

filtro = ('alert.','<script>','<','>','javascript',';','--',",","=","+",'/',"'",'"',"src=","admin'--"
            ,"or 1=1", "delete from usuario", "document.write","sessionStorage.","Window.","document.",'href=',"]>")

def get_usuario(email: str):
    return Usuario.query.get(email)

def verifica_injecao(dado: str):
    for f in filtro: # laço de repetição que verifica se não há um texto suspeito de possuir injeção XSS ou SQL.
        if f in dado:
            resposta = dado.replace(f,'')
    if dado == '':
        resposta = None
    return resposta

def verifica_injecao_email(email: str):
    for f in filtro: # laço de repetição que verifica se não há um texto suspeito de possuir injeção XSS ou SQL.
        if f in email:
            resposta = email.replace(f,'')
    if resposta == '' and len(resposta)<=4 or '@' not in resposta:
        resposta = None
    return resposta

def atualizar_user(dados):
    try:
        user = get_usuario(dados['email'])
        if user is not None:
            if 'novo_nome' in dados:
                user.nome = verifica_injecao(dados['novo_nome'])
            if 'senha' in dados:
                senha = verifica_injecao(dados['senha']) 
                user.senha = criptografar_sen(senha) 
            if 'email' in dados:
                user.email = verifica_injecao_email(dados['novo_email'])
            if dados is None:
                return False
            db.session.commit()
            return True
        return False
    except Exception as e:
        return False

def verifica_senha(senha_dig:str,email_dig:str):
    """Verifica se a senha digitada corresponde a que está no banco de dados
    usando o email de referência.

    Args:
        senha_dig (str): senha digitada.
        email_dig (str): email digitado.

    Returns:
        bool: False caso aconteça algum erro ou caso a senha não esteja no banco;
        True caso esteja e corresponda.
    """
    for q in db.session.query(Usuario.senha).filter(Usuario.id_email==email_dig).all():
        try:
            resultado = bcrypt.checkpw(senha_dig,q[0])
        except:
            return False
        if q == None:
            return False
        return resultado