import bcrypt

senha = "Lucas@123456"
senha_utf8 = senha.encode('utf-8')
aaa = 'aaaaa'
aaa_utf8 = aaa.encode('utf-8')

# gera hash
hash_da_senha = bcrypt.hashpw(senha_utf8,bcrypt.gensalt())
# compara o hash com a normal
conf = bcrypt.checkpw(aaa_utf8, hash_da_senha)

print(hash_da_senha)
print(conf)