from bcrypt import *
from flask import *
from config import *
from modelo import *

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/cadastro', methods=['POST'])
def cadastro():
    
    resposta = jsonify({'Resultado': 'ok', 'Detalhes': 'ok'})
    dados = request.get_json()

    try:
        usuario = Usuario(**dados)
        usuario.criptografar_sen()
        db.session.add(usuario)
        db.session.commit()
    except Exception as e:
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
    
    resposta.headers.add('Access-Control-Allow-Origin', '*')

    return resposta

@app.route('/render_niveis', methods=['GET'])
def niveis():
    return render_template('niveis.html')


@app.route('/login', methods= ['POST'])
def login():
    resposta = jsonify({'Resultado': 'ok', 'Detalhes': 'ok'})
    resposta.headers.add('Access-Control-Allow-Origin', '*')
    try:
        dados = request.get_json()
        email = dados['email']
        print(email)
        senha = dados['senha']
        print(senha)
        senha = senha.encode('utf-8')
        login = verifica_senha(senha,email)
        print(login)
        
        if login == False:
            redirect(url_for('login'))
            raise Exception
        return redirect(url_for('index'))
    except Exception as e:
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        return resposta

@app.route('/ranking', methods=['GET'])
def ranking():
    try:
        ranking_users = db.session.query(Usuario).all()
        users_json = [x.json for x in ranking_users]
        resposta = jsonify(users_json)
        resposta.headers.add('Access-Control-Allow-Origin', '*')
    except Exception as e:
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})

    return resposta

@app.route('/update_nome', methods=['POST'])
def update_nome():
    try:
        user = ''
        email_user = ''
        db.query(Usuario).filter_by(email=email_user).update(dict(nome=user))
        db.session.commit()
        resposta = jsonify({'Resultado': 'ok'})
    except Exception as e:
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        resposta.headers.add('Access-Control-Allow-Origin', '*')
        return resposta
    return resposta

@app.route('/update_email', methods=['POST'])
def update_email():
    try:
        email_user = ''
        db.query(Usuario).filter_by(email=email_user).update(dict(email=email_user))
        db.session.commit()
        resposta = jsonify({'Resultado': 'ok'})
    except Exception as e:
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        resposta.headers.add('Access-Control-Allow-Origin', '*')
        return resposta
    return resposta

@app.route('/update_senha', methods=['POST'])
def update_senha():
    try:
        email_user = ''
        senha_new = ''
        senha_new = senha_new.encode('utf-8')
        senha_new = bcrypt.hashpw(senha_new,bcrypt.gensalt()) 
        db.query(Usuario).filter_by(email=email_user).update(dict(senha=senha_new))
        db.session.commit()
    except Exception as e:
        resposta = jsonify({'Resultado': 'Erro', 'Detalhes': str(e)})
        resposta.headers.add('Access-Control-Allow-Origin', '*')
        return resposta
    return resposta

if __name__ == '__main__':
    
    app.run(debug=True)
    