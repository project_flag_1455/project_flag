from config import *
import bcrypt

class Desafio(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    nome = db.Column(db.String(254))
    password = db.Column(db.String(254))
    pont_ger = db.Column(db.Integer)
    pont_cript = db.Column(db.Integer)
    pont_estgn = db.Column(db.Integer)
    pont_char = db.Column(db.Integer)

    def __str__(self):
        return f'{self.id},{self.nome},{self.password}'

class Usuario(db.Model):
    id_email = db.Column(db.String(254),primary_key=True)
    nome = db.Column(db.String(254))
    senha = db.Column(db.String(254))
    pont_ger = db.Column(db.Integer,nullable = True)
    pont_cript = db.Column(db.Integer,nullable = True)
    pont_estgn = db.Column(db.Integer,nullable = True)
    pont_char = db.Column(db.Integer,nullable = True)
    desafio_atual = db.Column(db.Integer,db.ForeignKey(Desafio.id),nullable=False)
    

    def __str__(self):
        return f"{self.id_email},{self.nome},{self.senha},{self.pont_ger},{self.pont_cript},{self.pont_estgn},{self.pont_char}"

    def json(self):
        return {
            'email':self.id_email,
            'nome':self.nome
        }
    
    def ret_pont(self):
        return{
            'pont_ger':self.pont_ger,
            'pont_cript':self.pont_cript,
            'pont_estgn':self.pont_estgn,
            'pont_char':self.pont_char
        }


    def criptografar_sen(self):
        self.senha = self.senha.encode('utf-8')
        self.senha = bcrypt.hashpw(self.senha,bcrypt.gensalt())


def rankea_json():
    for r in db.query(Usuario).filter().first(20):
        r_json += {
            'nome':r[2],
            'pont_ger':r[4]
        }
    return r 

def verifica_senha(senha_dig:str,email_dig:str):
    for q in db.session.query(Usuario.senha).filter(Usuario.id_email==email_dig).all():
        try:
            resultado = bcrypt.checkpw(senha_dig,q[0])
        except:
            return False
        if q == None:
            return False
        return resultado


if __name__ == '__main__':
    #db.create_all()
    pessoa = Usuario(id_email = '1',nome = '1', senha = '1', desafio_atual = 1)
    pessoa.criptografar_sen()
    #db.session.add(pessoa)
    #db.session.commit()

